#include "model/bth_info.h"
#include "model/bth_scanner.h"

int main()
{
    printf("Prepare app for searching...\n");

    bth_scanner scanner;

    printf("Start searching Bluetooth devices...\n");

    bth_info binfo;
    for (int i = 0; i < scanner.num_rsp; i++) {

        if (i == 0)
            printf("====================================\n");        

        binfo.set_addr(scanner.ii[i]);
        binfo.set_name(scanner.ii[i], scanner.sock);

        printf("%s  %s\n", binfo.get_addr(), binfo.get_name());

        if (i == scanner.num_rsp - 1)
            printf("====================================\n");
    }

    if (scanner.num_rsp == 0)
        printf("No Bluetooth devices were found\n");

    printf("End of searching.\n");    
    
    return 0;
}