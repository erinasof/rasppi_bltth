#include "../constants.h"
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

class bth_info {

    char bt_addr[ADDR_LEN];
    char bt_name[NAME_LEN];
public:
    bth_info();
    void set_addr(inquiry_info inq_info);
    void set_name(inquiry_info inq_info, int sock);
    char* get_addr();
    char* get_name();
};