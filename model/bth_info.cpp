#include "bth_info.h"

bth_info::bth_info() : bt_addr{0}, bt_name{0} {}

void bth_info::set_addr(inquiry_info inq_info) {
    ba2str(&(inq_info.bdaddr), bt_addr);
}

void bth_info::set_name(inquiry_info inq_info, int sock) {
    memset(bt_name, 0, NAME_LEN);
    if (hci_read_remote_name(sock, &(inq_info.bdaddr), NAME_LEN, bt_name, 0) < 0)
        strcpy(bt_name, "{UNKNOWN}");
}

char* bth_info::get_addr() {        
    return bt_addr;
}

char* bth_info::get_name() {        
    return bt_name;
}