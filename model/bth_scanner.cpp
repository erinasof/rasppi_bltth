#include "bth_scanner.h"
#include <unistd.h>
#include <stdlib.h>

bth_scanner::bth_scanner() {    
    dev_id = hci_get_route(NULL);
    sock = hci_open_dev(dev_id);
    if (dev_id < 0 || sock < 0) {
        perror("opening socket");
        exit(1);        
    }

    ii = new inquiry_info[MAX_RSP];

    num_rsp = hci_inquiry(dev_id, LEN, MAX_RSP, NULL, &ii, IREQ_CACHE_FLUSH);
    if (num_rsp < 0)
        perror("hci_inquiry");    
}

bth_scanner::~bth_scanner() {
    delete[] ii;
    close(sock);
}