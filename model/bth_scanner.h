#include "../constants.h"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

class bth_scanner{
public:
    int dev_id;
    int sock;
    int num_rsp;
    inquiry_info *ii;

    bth_scanner();
    ~bth_scanner();
};