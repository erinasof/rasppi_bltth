# rasppi_bltth

## Description

Консольное приложение на С++ для микрокомпьютера Raspberry Pi 4 с Raspberry Pi OS, которое выполняет сканирование bluetooth-устройств и выводит их список.

### Build with command

Запустить сборку, находясь в родительской директории относительно каталога проекта

g++ -o bltth-search -g rasppi_bltth/\*\*.cpp rasppi_bltth/model/\*\*.cpp  -lbluetooth

### Run app

./bltth-search